#!/usr/bin/env bash
#
# Use curl to hit an endpoint on a service in order to do a Health Check
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

# Required parameters
URL=${URL:?'URL variable missing.'}

# Default parameters
RETRIES=${RETRIES:="3"}
RETRY_WAIT=${RETRY_WAIT:="5"}
DEBUG=${DEBUG:="false"}
case ${RETRIES} in
  ''|*[!0-9]*) fail "RETRIES must be an integer value \> 0" ;;
  *) ;;
esac
case ${RETRY_WAIT} in
  ''|*[!0-9]*) fail "RETRY_WAIT must be an integer value \> 0" ;;
  *) ;;
esac

enable_debug

health_check() {
  COUNT=${RETRIES}
  while [ ${COUNT} -ne "0" ]
  do
    HTTP=`curl -s -o /dev/null -w "%{http_code}" ${URL}`
    info HTTP GET ${URL} : ${HTTP}
    if [ "${HTTP}" == "200" ]
    then
      return 0
    fi
    COUNT=`expr ${COUNT} - 1`
    if [ ${COUNT} -ne "0" ]
    then
      sleep ${RETRY_WAIT}
    fi
  done
  if [ -z "${HTTP}" -o "${HTTP}" -eq "0" ]
  then
    HTTP=9999
  fi
  return ${HTTP}
}

run health_check

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Failed to get a 200/OK from ${URL}!"
fi
