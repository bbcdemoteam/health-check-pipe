#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/health-check-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
    run docker run \
        -e URL="http://www.google.com" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

