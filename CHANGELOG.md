# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.3

- patch: Fix when curl fails and gets a 000 http code

## 0.1.2

- patch: Add curl in dockerfile
- patch: Fix return code/exit code

## 0.1.1

- patch: Add curl to docker image

## 0.1.0

- minor: Initial release

