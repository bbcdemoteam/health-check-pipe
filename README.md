# Bitbucket Pipelines Pipe: Health Check

Use curl to hit an endpoint on a service in order to do a Health Check

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: bbcdemoteam/health-check-pipe:0.1.3
    variables:
      URL: "<string>"
      RETRIES: "<integer>"
      RETRY_WAIT: "<integer>"
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| URL (*)               | The health check endpoint to hit |
| RETRIES               | The number of retries to do in case of an error (default 3) |
| RETRY_WAIT            | The number of seconds to wait in between retries (default 5) |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Examples

Basic example:

```yaml
script:
  - pipe: bbcdemoteam/health-check-pipe:0.1.3
    variables:
      URL: "https://myservice.com/healthcheck"
```

Advanced example:

```yaml
script:
  - pipe: bbcdemoteam/health-check-pipe:0.1.3
    variables:
      URL: "https://myservice.com/healthcheck"
      RETRIES: "10"
      RETRY_WAIT: "10"
      DEBUG: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,health-check-pipe).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
